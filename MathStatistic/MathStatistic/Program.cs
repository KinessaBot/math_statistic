﻿using System;
using System.Collections.Generic;
using MathNet.Numerics.Statistics;


namespace MathStatistic
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input size of data");
            try
            {
                int n = Convert.ToInt16(Console.ReadLine());
                List<double> data = new List<double>();
                Console.WriteLine("Input data");
                for (int i = 0; i < n; i++)
                {
                    data.Add(Convert.ToDouble(Console.ReadLine()));
                }
                int choose = 0;
                while (choose != 6)
                {
                    Console.WriteLine(
                    "Choose operation:\n" +
                    "1 Maximum and minimum\n" +
                    "2 Mean\n" +
                    "3 Median\n" +
                    "4 Variance\n" +
                    "5 Quantile\n" +
                    "6 Exit"
                    );
                    choose = Convert.ToInt16(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            {
                                Console.WriteLine(data.Maximum() + "; " + data.Minimum());
                                break;
                            }
                        case 2:
                            {
                                Console.WriteLine(data.Mean());
                                break;
                            }
                        case 3:
                            {
                                Console.WriteLine(data.Median());
                                break;
                            }
                        case 4:
                            {
                                Console.WriteLine(data.Variance());
                                break;
                            }
                        case 5:
                            {
                                Console.WriteLine("Input tau [0,0 - 1,0]");
                                Console.WriteLine(data.Quantile(Convert.ToDouble(Console.ReadLine())));
                                break;
                            }
                        default:
                            {
                                return;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
